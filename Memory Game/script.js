window.onload = function () {
    var imgs = document.querySelectorAll(".memory-table tr td img");

    for (var i = 0; i < imgs.length; i++){
        imgs[i].onclick = showCard;
    }

    lucky();
    addIDs(imgs);
}    

var cardsIDs = [];
var clickedIDs = [];
//Contador de Cliques (abaixo)
var counter = 0;
var pairs = [
    [1,  2],
    [3,  4],
    [5,  6],
    [7,  8],
    [9,  10],
    [11, 12],
    [13, 14],
    [15, 16],
    [17, 18],
    [19, 20],
    [21, 22],
    [23, 24]
];
var usedIDs = [];

function showCard(e){
    this.src = "img/cards/" + this.id + ".png";
    clickedIDs.push(this.id);    
    counter++;
    if(counter == 2){
        var imgA = document.getElementById(clickedIDs[0]);
        var imgB = document.getElementById(clickedIDs[1]);

        if(check() == true){
            imgA.style.opacity = 0.2;
            imgB.style.opacity = 0.2;
            alert("P-A-R-A-B-É-N-S");
        }
        else{
            setTimeout(function(){
                imgA.src = "img/default/background.png";
                imgB.src = "img/default/background.png";
              }, 200);
            alert("Tente Novamente");           
        }    
        counter = 0;
        clickedIDs = [];         
    }
}

function lucky(){
	while(cardsIDs.length < 24){
        
        var id = Math.floor(Math.random() * 24)+1;
        
        console.log(id);
        
        if(cardsIDs.indexOf(id) == -1)
			cardsIDs.push(id);
    }    
}

function addIDs(elements){
    for(var i = 0; i < elements.length; i++){
        elements[i].id = cardsIDs[i]
   }
}


function check(){
    var idA = clickedIDs[0];
    var idB = clickedIDs[1];
    var matchID = -1;
    usedIDs.push(idA);
    usedIDs.push(idB);

    for(var i = 0; i < pairs.length; i++){

        if(idA == pairs[i][0]){
            matchID = pairs[i][1];
            break;
        }
        else if(idA == pairs[i][1]){
            matchID = pairs[i][0];
            break;
        }    
    }

    if(matchID == idB){
        return true;
    }
    else{
        return false;
    }


}

